# Honor cup

Решение предоставил Краюхин А.С.

Характеристики компьютера, где выполнялось обучение:

- ОС: Ubuntu 20.04 lts
- CUDA Version: 11.2
- Driver Version: 460.27.04
- GPU: Nvidia Rtx 2080 Super
- Python 3.6.12
- pip 20.0.2

### Подготовка рабочей среды

Для запуска решения используются различные python-библиотеки
для установки необходимых библиотек необходимо выполнить ряд действий:

- Подготовить виртуальное окружение

Windows:
```shell
\> python-m venv ven
\> venv\Script\activate
```

Linux:
```bash
$ python3.6 -m venv ven
$ source venv/bin/activate
```

- Утановить требуемые Python-пакеты

```bash
$ pip install -U pip
$ pip install -r requirements.txt
```

### Запуск скрипта на генерацию Low-Resolution изображений

Для запуска скрипта на генерацию Low-resolution изображений необходимо 
выполнить ряд команд:

```bash
$ cd solution
$ python -W ignore run.py --no-training
$ cd ..
```

В результате выполнения скрипта в директории solution/test/LR образуются
сгенерированные Low Resolution изображения


### Запуск скрипта на генерацию High-Resolution изображений

Для запуска скрипта на генерацию High-Resolution изображений необходимо 
выполнить ряд команд:

```bash
$ cd score_script
$ python -W ignore run.py ../train/HR ../train/LR ../test/HR ../solution/test/LR
$ cd ..
```

В результате будет выведено полученное значение DISTS-метрики, а также
сгененрированны изоражения High-Resolution изображения, помещенные в директорию
score_script/results

### Полученные результаты

В ходе проведения тестирования разработанного решения получены следующие результаты:

DISTS value         | Conv layers number**  | Model weights path***
------------------- | --------------------- | --------------------------------------
0.14681333303451538 | 0                     | None
0.15995141863822937 | 5                     | solution/saved_models/model_5_conv.pth

** количество сверточных слоев указано в файле solution/config.yml

*** для указания фалйа весов при запуске скрипта solution/run.py необходимо указать флага
weights и соответсвующим значением