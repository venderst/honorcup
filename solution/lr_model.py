from torch import nn


class LRNet(nn.Module):

    def __init__(self, conv_layers_num: int):
        super(LRNet, self).__init__()

        assert conv_layers_num >= 0, 'conv_layes_num must be >= 0'
        self.net = nn.Sequential()
        # the number of convolutional layers before/after the pooling layer,
        # the total number is twice as large
        for scale_rate in range(conv_layers_num):
            self.net.add_module(f'conv_{scale_rate}', nn.Conv2d(
                in_channels=3*2**scale_rate,
                out_channels=3*2**(scale_rate + 1),
                kernel_size=3, stride=1, padding=1
            ))
            self.net.add_module(f'elu_{scale_rate}', nn.ELU())
        self.net.add_module('pool_1', nn.AvgPool2d(
            kernel_size=2, stride=2
        ))
        for scale_rate in range(conv_layers_num, 0, -1):
            self.net.add_module(
                f'conv_{2 * conv_layers_num - scale_rate}',
                nn.Conv2d(
                    in_channels=3*2**scale_rate,
                    out_channels=3*2**(scale_rate - 1),
                    kernel_size=3, stride=1, padding=1
                )
            )
            self.net.add_module(f'elu_{scale_rate}', nn.ELU())

    def forward(self, x):
        return self.net(x)
