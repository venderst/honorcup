from abc import ABC, abstractmethod
from typing import Callable, Optional, Text
import os

import torch


class Callback(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def __call__(self, model, train_loss: float, val_loss: float, **kwargs):
        pass


class ModelCheckpoint(Callback):

    def __init__(self, monitor: Optional[Text] = 'val_loss',
                 comparing_fn: Optional[Callable] = None,
                 saved_models_folder_path: Optional[Text] = 'saved_models',
                 saved_model_name_pattern: Optional[Text] = None,
                 save_best_only: Optional[bool] = True) -> None:
        super(ModelCheckpoint, self).__init__()
        self._monitor = monitor
        self._comparing_fn = comparing_fn or max
        self._best_monitor_value = None
        self._saved_models_folder_path = saved_models_folder_path
        self._saved_model_name_pattern = saved_model_name_pattern or \
            'model.pth'
        self._save_best_only = save_best_only
        self.best_saved_model_path = None
        os.makedirs(saved_models_folder_path, exist_ok=True)

    def __call__(self, model, train_loss: float, val_loss: float, **kwargs):
        kwargs = kwargs.get('kwargs', kwargs)
        kwargs.update({
            'train_loss': train_loss,
            'val_loss': val_loss
        })
        monitor_value = kwargs[self._monitor]
        if (not self._best_monitor_value or
           self._comparing_fn(monitor_value, self._best_monitor_value) ==
           monitor_value):
            self._best_monitor_value = monitor_value
            if self.best_saved_model_path and self._save_best_only:
                os.remove(self.best_saved_model_path)
            self.best_saved_model_path = os.path.join(
                self._saved_models_folder_path,
                self._saved_model_name_pattern.format(**kwargs)
            )
            torch.save(model.state_dict(), self.best_saved_model_path)
