from typing import Text, Callable, Tuple, List
import random

import torch
from torch import Tensor

__all__ = ['Compose', 'FixedCrop', 'RandomCrop']


class Compose(object):
    """Consistently applies the specified transformations
    Args:
        transforms (List): sequence of applied transformations
    """

    def __init__(self, transforms: List[
        Callable[[Tensor, Tensor], Tuple[Tensor, Tensor]]]) -> None:
        self._transforms = transforms

    def __call__(self, lr_image: Tensor, hr_image: Tensor
                 ) -> Tuple[Tensor, Tensor]:
        """
        Args:
            lr_image (torch.Tensor): LR image to be cropped
            hr_image (torch.Tensor): HR image to be cropped.
        Returns:
            Tuple[Tensor, Tensor] cropped LR and HR images
        """
        for transform in self._transforms:
            lr_image, hr_image = transform(lr_image, hr_image)

        return lr_image, hr_image

    def __repr__(self) -> Text:
        return f'{self.__class__.__name__}(' \
               f'transforms={self._transforms})'


class FixedCrop(object):
    """Crop the given images at a random location with fixed crop size.
    Args:
        crop_size (int): num pixels of image be cropped in LR image
    """

    def __init__(self, crop_size: int) -> None:
        assert crop_size > 0, 'crop_size must be more that 0'
        self._crop_size = crop_size

    def __call__(self, lr_image: Tensor, hr_image: Tensor
                 ) -> Tuple[Tensor, Tensor]:
        """
        Args:
            lr_image (torch.Tensor): LR image to be cropped
            hr_image (torch.Tensor): HR image to be cropped.
        Returns:
            Tuple[Tensor, Tensor] cropped LR and HR images
        """
        crop_size = min(self._crop_size, lr_image.shape[1], lr_image.shape[2])
        x_start = random.randint(0, lr_image.shape[1] - crop_size)
        y_start = random.randint(0, lr_image.shape[2] - crop_size)

        lr_image = lr_image[
                   :,
                   x_start:x_start + crop_size,
                   y_start:y_start + crop_size]
        hr_image = hr_image[
                   :,
                   x_start * 2:x_start * 2 + crop_size * 2,
                   y_start * 2:y_start * 2 + crop_size * 2]
        if crop_size < self._crop_size:
            result_lr_image = torch.zeros(3, self._crop_size, self._crop_size)
            result_lr_image[:, :crop_size, :crop_size] = lr_image
            result_hr_image = torch.zeros(3, self._crop_size * 2,
                                          self._crop_size * 2)
            result_hr_image[:, :crop_size*2, :crop_size*2] = hr_image
            return result_lr_image, result_hr_image
        return lr_image, hr_image

    def __repr__(self) -> Text:
        return f'{self.__class__.__name__}(' \
               f'crop_size={self._crop_size})'


class RandomCrop(object):
    """Crop the given images at a random location with random crop size.
    Args:
        min_crop_size (float): minimum part of the image that can be cropped,
                           value in range [0, 1)
        max_crop_size (float): maximum part of the image that can be cropped,
                               value in range (0, 1]
    """

    def __init__(self, min_crop_size: float, max_crop_size: float) -> None:
        assert 0 < max_crop_size <= 1, 'max_crop_size must be in range (0, 1]'
        assert 0 <= min_crop_size < 1, 'min_crop_size must be in range [0, 1)'
        assert min_crop_size <= max_crop_size, \
            'min_crop_size must be less that max_crop_size'
        self._min_crop_size = min_crop_size
        self._max_crop_size = max_crop_size

    def __call__(self, lr_image: Tensor, hr_image: Tensor
                 ) -> Tuple[Tensor, Tensor]:
        """
        Args:
            lr_image (torch.Tensor): LR image to be cropped
            hr_image (torch.Tensor): HR image to be cropped.
        Returns:
            Tuple[Tensor, Tensor] cropped LR and HR images
        """
        min_side_len = min(lr_image.shape[1], lr_image.shape[2])
        min_crop_size = int(self._min_crop_size * min_side_len)
        max_crop_size = int(self._max_crop_size * min_side_len)
        crop_size = random.randint(min_crop_size, max_crop_size)
        x_start = random.randint(0, lr_image.shape[1] - crop_size)
        y_start = random.randint(0, lr_image.shape[2] - crop_size)

        lr_image = lr_image[
                   :,
                   x_start:x_start + crop_size,
                   y_start:y_start + crop_size]
        hr_image = hr_image[
                   :,
                   x_start * 2:x_start * 2 + crop_size * 2,
                   y_start * 2:y_start * 2 + crop_size * 2]
        return lr_image, hr_image

    def __repr__(self) -> Text:
        return f'{self.__class__.__name__}(' \
               f'min_crop_size={self._min_crop_size}, ' \
               f'max_crop_size={self._max_crop_size})'
