import piq


class DISTSWrapper(object):

    def __init__(self, device):
        self._metric = piq.DISTS().to(device)

    def __call__(self, prediction, target):
        return self._metric(prediction, target)

    def __name__(self):
        return 'DISTS'

    def __repr__(self):
        return 'DISTS'
