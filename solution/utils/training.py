from typing import Optional, Callable, Any, List, Dict

import torch
from torch import Tensor
from torch.utils.data import Dataset, DataLoader
from tqdm.auto import tqdm

from lr_model import LRNet


__all__ = ['train_lr_model']


def train_lr_model(lr_model: LRNet,
                   optimizer: Any,
                   loss: Optional[Callable[[Any, Any], Tensor]],
                   train_dataset: Dataset,
                   val_dataset: Dataset,
                   epochs_num: int,
                   batch_size: int = 1,
                   num_workers: int = 0,
                   device: Optional[torch.device] = None,
                   metrics: Optional[List[Callable]] = None,
                   callbacks: Optional[List[Callable]] = None) -> List[Dict]:
    """
    Args:
        lr_model (LRNet): trained model
        optimizer (Any): used the optimizer
        loss (Callable): loss function that takes as input (prediction, target)
            and returns a loss value

        metrics (List[Callable]): a list of callables that take
            (prediction, target) as arguments and return a value
        callbacks (List[Callable]): a list of callables that accept (model,
            mean train loss, mean val loss,
            and ['train', 'val'] metric values as kwargs)
    """
    device = device or torch.device('cpu')
    lr_model = lr_model.to(device)

    train_dataloader = DataLoader(train_dataset, batch_size=batch_size,
                                  shuffle=True, num_workers=num_workers)
    val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True,
                                num_workers=1)

    metrics = metrics or []
    callbacks = callbacks or []

    training_history = []

    print('Starting LR model training')

    for epoch in range(1, epochs_num + 1):
        training_history.append({})

        print(f'Epoch {epoch}/{epochs_num} training')

        train_losses = []
        train_metric_values = {f'train_{metric}': [] for metric in metrics}
        train_generator = tqdm(train_dataloader)
        generator_postfix = {}
        lr_model.train()

        for lr_images, hr_images in train_generator:
            lr_images = lr_images.to(device)
            hr_images = hr_images.to(device)

            if len(lr_images.shape) == 3:
                lr_images = torch.unsqueeze(lr_images, dim=0)
            if len(hr_images.shape) == 3:
                hr_images = torch.unsqueeze(hr_images, dim=0)

            optimizer.zero_grad()
            prediction = lr_model(hr_images)
            loss_value = loss(prediction, lr_images)
            loss_value.backward()
            optimizer.step()

            train_losses.append(loss_value.item())
            generator_postfix['train_loss'] = loss_value.item()

            for metric in metrics:
                train_metric_values[f'train_{metric}'].append(metric(
                    prediction, lr_images).item())
                generator_postfix[f'train_{metric}'] = \
                    train_metric_values[f'train_{metric}'][-1]
            train_generator.set_postfix(generator_postfix)
        train_loss = torch.tensor(train_losses)
        training_history[-1]['train'] = {
            'min loss': train_loss.min().item(),
            'avg loss': train_loss.mean().item(),
            'max loss': train_loss.max().item()
        }
        train_loss = train_loss.mean().item()
        train_metric_values = {
            metric: torch.tensor(metric_values).mean().item()
            for metric, metric_values in train_metric_values.items()
        }
        training_history[-1]['train'].update(train_metric_values)
        generator_postfix['train_loss'] = train_loss
        generator_postfix.update(train_metric_values)
        train_generator.set_postfix(generator_postfix)

        print(f'Epoch {epoch}/{epochs_num} validation')

        val_losses = []
        val_metric_values = {f'val_{metric}': [] for metric in metrics}
        val_generator = tqdm(val_dataloader)
        lr_model.eval()

        for lr_images, hr_images in val_generator:
            lr_images = lr_images.to(device)
            hr_images = hr_images.to(device)

            if len(lr_images.shape) == 3:
                lr_images = torch.unsqueeze(lr_images, dim=0)
            if len(hr_images.shape) == 3:
                hr_images = torch.unsqueeze(hr_images, dim=0)

            prediction = lr_model(hr_images)
            loss_value = loss(prediction, lr_images)

            val_losses.append(loss_value.item())
            generator_postfix['val_loss'] = loss_value.item()

            for metric in metrics:
                val_metric_values[f'val_{metric}'].append(
                    metric(prediction, lr_images).item())
                generator_postfix[f'val_{metric}'] = \
                    val_metric_values[f'val_{metric}'][-1]
            val_generator.set_postfix(generator_postfix)
        val_loss = torch.tensor(val_losses)
        training_history[-1]['val'] = {
            'min loss': val_loss.min().item(),
            'avg loss': val_loss.mean().item(),
            'max loss': val_loss.max().item()
        }
        val_loss = val_loss.mean().item()
        val_metric_values = {
            metric: torch.tensor(metric_values).mean().item()
            for metric, metric_values in val_metric_values.items()
        }
        training_history[-1]['val'].update(val_metric_values)
        generator_postfix['val_loss'] = train_loss
        generator_postfix.update(val_metric_values)
        val_generator.set_postfix(generator_postfix)

        kwargs = train_metric_values
        kwargs.update(val_metric_values)

        for callback in callbacks:
            callback(lr_model, train_loss, val_loss, kwargs=kwargs)
    return training_history
