from typing import Text, Optional, List, Callable, Tuple
import os
import shutil

from PIL import Image
from torch import Tensor
import torchvision.transforms as TF
from torch.utils.data import Dataset

__all__ = ['LRDataset', 'copy_and_split_dataset']


class LRDataset(Dataset):
    """LR images dataset"""

    def __init__(
            self, hr_images_folder_path: Text,
            lr_images_folder_path: Text,
            transforms: Optional[
                Callable[[Tensor, Tensor], Tuple[Tensor, Tensor]]] = None,
            length: Optional[int] = None) -> None:
        """
        Args:
            hr_images_folder_path (Text): is a path to folder with HR images
            lr_images_folder_path (Text): is a path to folder with LR images
            transforms (Callable): is a callable,
                where the first parameter is lr_image,
                the second parameter is hr_image
            length (int): is a max generated elements number
                (use it when transfroms are given)
        """
        hr_images_files_names = os.listdir(hr_images_folder_path)
        lr_images_files_names = os.listdir(lr_images_folder_path)

        assert hr_images_files_names == hr_images_files_names, \
            'hr and lr files names must be equals'

        self._filenames = lr_images_files_names
        self._lr_images_folder_path = lr_images_folder_path
        self._hr_images_folder_path = hr_images_folder_path

        self._transforms = transforms
        self._to_tensor_transform = TF.ToTensor()
        self._length = length

    def __len__(self) -> int:
        return self._length or len(self._filenames)

    def __getitem__(self, index: int) -> Tuple[Tensor, Tensor]:
        if index >= self.__len__():
            raise StopIteration
        image_file_name = self._filenames[index % len(self._filenames)]

        lr_image = Image.open(os.path.join(
            self._lr_images_folder_path, image_file_name))
        hr_image = Image.open(os.path.join(
            self._hr_images_folder_path, image_file_name))

        lr_image = self._to_tensor_transform(lr_image)
        hr_image = self._to_tensor_transform(hr_image)

        lr_image_shape = tuple(lr_image.shape)
        hr_image_shape = tuple(hr_image.shape)
        lr_image_scaled_shape = (
            lr_image_shape[0], lr_image_shape[1] * 2, lr_image_shape[2] * 2)

        if hr_image_shape != lr_image_scaled_shape:
            raise RuntimeError(
                'Shapes of LR and HR images mismatch for sample '
                f'{image_file_name}')

        if self._transforms:
            lr_image, hr_image = self._transforms(lr_image, hr_image)
        return lr_image, hr_image


def move_patr_files(src_folder_path: Text, target_folder_path: Text,
                    moved_files_part: Optional[float] = None,
                    moved_files_names: Optional[List] = None,
                    moved_files_type: Optional[Text] = 'png') -> List:
    """
    Moves the specified part of the images to a new directory

    :return moved files names
    """
    assert moved_files_part or moved_files_names, \
        'moved_files_part or moved_files_names must be specified'
    if not moved_files_names:
        src_folder_files = [
            filename for filename in os.listdir(src_folder_path)
            if filename.endswith(moved_files_type)
        ]
        moved_imgs_num = int(len(src_folder_files) * moved_files_part)
        moved_files_names = src_folder_files[:moved_imgs_num]
    os.makedirs(src_folder_path, exist_ok=True)
    os.makedirs(target_folder_path, exist_ok=True)
    for filename in moved_files_names:
        prev_file_path = os.path.join(src_folder_path, filename)
        new_file_path = os.path.join(target_folder_path, filename)
        shutil.move(prev_file_path, new_file_path)
    return moved_files_names


def copy_and_split_dataset(src_train_images_folder_path: Text,
                           src_test_images_folder_path: Text,
                           target_train_folder_path: Optional[Text] = 'train',
                           target_test_folder_path: Optional[Text] = 'test',
                           target_val_folder_path: Optional[Text] = 'val',
                           val_size: Optional[int] = 0.25
                           ) -> None:
    # Copying directories to separate selections
    print('Copying dataset')
    if os.path.exists(target_train_folder_path):
        shutil.rmtree(target_train_folder_path)
    if os.path.exists(target_test_folder_path):
        shutil.rmtree(target_test_folder_path)
    if os.path.exists(target_val_folder_path):
        shutil.rmtree(target_val_folder_path)
    shutil.copytree(src_train_images_folder_path, target_train_folder_path)
    shutil.copytree(src_test_images_folder_path, target_test_folder_path)

    # Train-val splitting
    print('Train-val_splitting')
    train_lr_images_folder_path = os.path.join(target_train_folder_path, 'LR')
    train_hr_images_folder_path = os.path.join(target_train_folder_path, 'HR')

    val_lr_images_folder_path = os.path.join(target_val_folder_path, 'LR')
    val_hr_images_folder_path = os.path.join(target_val_folder_path, 'HR')

    moved_files_names = move_patr_files(
        train_lr_images_folder_path, val_lr_images_folder_path,
        moved_files_part=val_size
    )
    move_patr_files(
        train_hr_images_folder_path, val_hr_images_folder_path,
        moved_files_names=moved_files_names
    )
