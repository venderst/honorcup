from typing import Callable, Text
import argparse
import os
import json
from pathlib import Path
import random

import numpy as np
import torch
import torchvision.transforms.functional as TF
from PIL import Image, ImageFile
from ruamel.yaml import YAML
from tqdm.auto import tqdm

from lr_model import LRNet
from utils.data import *
from utils.transforms import *
from utils.training import train_lr_model
from utils.metrics import DISTSWrapper
from utils.callbacks import ModelCheckpoint

ImageFile.LOAD_TRUNCATED_IMAGES = True


def process_test_images(test_folder_path: Text, model: Callable,
                        device: torch.device) -> None:
    print('Starting test images processing')

    hr_images_folder_path = os.path.join(test_folder_path, 'HR')
    lr_images_folder_path = os.path.join(test_folder_path, 'LR')
    os.makedirs(lr_images_folder_path, exist_ok=True)
    for img_file_name in tqdm(os.listdir(hr_images_folder_path)):
        hr_img_file_path = \
            f'{hr_images_folder_path}{os.sep}{img_file_name}'
        lr_img_file_path = \
            f'{lr_images_folder_path}{os.sep}{img_file_name}'

        hr_img = TF.to_tensor(Image.open(hr_img_file_path)).to(device)
        hr_img = torch.unsqueeze(hr_img, dim=0)

        lr_img = model(hr_img)[0]
        lr_img = TF.to_pil_image(lr_img)
        lr_img.save(lr_img_file_path, "PNG")


def main() -> None:
    config_file_path = Path('config.yml')
    yaml_parser = YAML(typ='safe')
    config = yaml_parser.load(config_file_path)

    # random seed
    random.seed(config['random_seed'])
    np.random.seed(config['random_seed'])
    torch.manual_seed(config['random_seed'])
    torch.cuda.manual_seed(config['random_seed'])
    torch.backends.cudnn.deterministic = True

    # Arguments parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--weights', default=None,
        help='Path to the file with LRNet weights'
    )
    parser.add_argument(
        '--train-', dest='train_lr_model', action='store_true',
        help='Indicates that LRNet training is required'
    )
    parser.add_argument(
        '--no-training', dest='train_lr_model', action='store_false',
        help='Indicates that LRNet training is not required'
    )
    parser.set_defaults(train_lr_model=True)
    args = parser.parse_args()

    # Train-val split
    src_train_images_folder_path = config['src_train_images_folder_path']
    src_test_images_folder_path = config['src_test_images_folder_path']
    target_train_folder_path = config['target_train_images_folder_path']
    target_test_folder_path = config['target_test_images_folder_path']
    target_val_folder_path = config['target_val_images_folder_path']

    copy_and_split_dataset(
        src_train_images_folder_path, src_test_images_folder_path,
        target_train_folder_path, target_test_folder_path,
        target_val_folder_path, val_size=config['val_size']
    )

    train_transforms = []
    config_transforms = config['lr_model']['training']['transforms'].items()
    for transform, kwargs in config_transforms:
        train_transforms.append(eval(transform)(**kwargs))

    train_dataset = LRDataset(
        hr_images_folder_path=f'train{os.sep}HR',
        lr_images_folder_path=f'train{os.sep}LR',
        transforms=Compose(train_transforms),
        length=config['lr_model']['training']['training_set_length']
    )
    val_dataset = LRDataset(
        hr_images_folder_path=f'val{os.sep}HR',
        lr_images_folder_path=f'val{os.sep}LR'
    )

    # Device
    if config['device'] == 'auto':
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    else:
        device = torch.device(config['device'])
    print('Used device:', device)

    # LR model
    lr_model = LRNet(conv_layers_num=config['lr_model']['conv_layers_num'])
    if args.weights:
        print(
            'Loading LRNet weights from', args.lr_model_weights
        )
        lr_model.load_state_dict(
            torch.load(args.lr_model_weights)
        )
    lr_model = lr_model.to(device)
    lr_model_params = list(lr_model.parameters())

    if args.train_lr_model and lr_model_params:
        optimizer = torch.optim.Adam(lr_model.parameters())
        loss = torch.nn.L1Loss()
        metrics = [DISTSWrapper(device)]
        saved_model_name_pattern = config['lr_model']['training'][
            'saved_model_name_pattern']
        model_checkpoint = ModelCheckpoint(
            monitor='val_DISTS', comparing_fn=min,
            saved_model_name_pattern=saved_model_name_pattern,
            save_best_only=config['lr_model']['training']['save_best_only']
        )
        callbacks = [model_checkpoint]
        training_history = train_lr_model(
            lr_model, optimizer, loss, train_dataset, val_dataset,
            epochs_num=config['lr_model']['training']['training_epochs_num'],
            batch_size=config['lr_model']['training']['batch_size'],
            num_workers=config['lr_model']['training']['num_workers'],
            device=device, metrics=metrics,
            callbacks=callbacks
        )
        history_file_name = \
            config['lr_model']['training']['training_history_file_name']
        with open(history_file_name, 'w', encoding='utf-8') as json_file:
            json.dump(
                training_history, json_file, ensure_ascii=False, indent=4
            )
        best_model_path = model_checkpoint.best_saved_model_path
        lr_model.load_state_dict(torch.load(best_model_path))
        print(f'Loaded model {best_model_path}')
    else:
        print('Skipping lr_model training')

    process_test_images(target_test_folder_path, lr_model, device)


if __name__ == '__main__':
    main()
    print('Ending solution script processing', end='\n\n')
